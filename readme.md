# Top Free Games - Race Challange

Implementation in javascript and HTML5 of a mini racing game.

## Prerequisites

Browser that supports the latest HTML5 features.

## Running

To run the game simply open the index.html in your favorite browser.


## Solution Outline

As just a simple game loop was provided in the template, many of the basic features of a game engine were implemented, but due to the limited time only their core is finished.

Furthermore, no assets were provided, and so free assets are being used as placeholders. Please view them only as a test of the features implemented.

#### Animated Sprites

Many may argue that graphics are the most important aspect of a game, and thus this was the most developed feature in my mini engine.

Currently it supports, preloading, playing animations with any pattern desired, looping animations, and a simple notify at the end a play.

The basics of local transform was also implemented, but as not all objects are properly structurized, only the player uses this feature for now.


#### Input

A simple input manager was implemented, so we define inputs in a "higher level", abstracting what an event is supposed to do, and so mapping multiple inputs to a single abstraction, facilitating deployment to multiple plataforms.

Currently there are only pressed and realeased state for the inputs, but implementing hold shouldnt be too hard. And moving from an event based design, helps so that the gameplay stays similar across multiple decices.

#### Player

As there are different kinds of obstacles, and each affects the player logic in a singular manner, I implemented simplified state design pattern, so we could change the player's behaviour seamlessly.

#### Obstacles

I designed an algorithm that segregates the playable board into areas that are free of obstacles, occupied or that the player can't reach due to the configuration of the cars.

This way we can spawn new obstacles without fear of making an impossible game.

Since multiple obstacles will rotate through the playfield, and they differ in minor things, an Object pool was also implemented, so we can improve performance by reducing garbage collections, and sys calls.


#### Background

I intended to make a tileset based procedural generated background, but unfortunately my time was limited. For now it is only a static background repeating forever.
 

#### Sound and HUD

These were barely worked on, and only the barebones are implemented.

#### Documentation

It is now 23:40 and I really cant document much further, so please contact me if any problem arise.