var effect = {
  sprite: {draw: $.noop},
  x: 0,
  y: 0,
  width: 60,
  height: 120,

  velY: 0,
  parent: null,

  init: function (effectName, x, y, width, height, parent, velY, loop) {
    this.id = ++nextObjectId;
    this.x = x;
    this.y = y;
    this.width = width || 60;
    this.height = height || 120;

    this.parent = parent || null;
    this.velY =  velY || 0;

    this.sprite = spritesManager.newAnimatedSprite(effectName, this.width, this.height, loop);
    if (!loop)
      this.sprite.registerNotify(_.bind(this.destroy, this));
  },

  draw: function(context) {
    this.sprite.startDraw(context, this.x, this.y);
    this.sprite.draw(context);
    this.sprite.endDraw(context);
  },

  update: function(deltaTime) {
    this.sprite.update(deltaTime);

    if (this.parent)
      this.y += this.velY * deltaTime;
    else
      this.y += (player.velY - this.velY) * deltaTime;

    if (!this.parent && this.y > Game.canvas.height)
      roadManager.removeRoadObject(this);
  },

  destroy: function () {
    if (this.parent) this.parent.removeChild(this.id);
    else roadManager.removeRoadObject(this);
  },
};

function newEffect(effectName, x, y, dWidth, dHeight, parent, velY, loop) {
  var obj = _.clone(effect);
  obj.init(effectName, x, y, dWidth, dHeight, parent, velY, loop);
  return obj;
} 