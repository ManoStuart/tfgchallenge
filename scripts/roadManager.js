var nextObjectId = 0;

var roadManager = {
  objects: {},
  backObjects: {},
  objectPool: [],

  curSpawnExtra: 10,

  spawnCooldown: 1.5,
  curSpawnCD: 0, 

  active: true,

  init: function(canvas, prealoc) {
    initObstacles();

    var bounds = road.getFirstBounds();
    this.roadStatus = newRoadArea(bounds[0], bounds[0] + bounds[1]);

    for (var i = 0; i < prealoc; i++)
      this.objectPool.push(newObstacle());
  },

  reset: function () {
    this.active = true;
    this.spawnCooldown = 1.5;
    var bounds = road.getFirstBounds();
    this.roadStatus = newRoadArea(bounds[0], bounds[0] + bounds[1]);

    this.objectPool.concat(_.toArray(this.objects));
    this.objects = {};
  },

  draw: function(context) {
    _.invoke(this.backObjects, 'draw', context);
    _.invoke(this.objects, 'draw', context);
  },

  update: function(deltaTime) {
    // Update objects
    _.invoke(this.backObjects, 'update', deltaTime);
    _.invoke(this.objects, 'update', deltaTime);

    // Spawn newObstacles
    if (this.active)
      this.checkSpawn(deltaTime);
  },

  checkSpawn: function(deltaTime) {
    // Update road status
    this.roadStatus.update();

    this.spawnCooldown -= 0.005 * deltaTime;
    this.spawnCooldown = this.spawnCooldown.clamp(0.4, 1000);

    this.curSpawnCD -= deltaTime;
    if (this.curSpawnCD < 0) {
      this.curSpawnCD = this.spawnCooldown;

      if (this.curSpawnExtra <= 0) {
        var rand = Math.random();
        var bounds = road.getFirstBounds();

        if (rand < 0.5) {
          this.addRoadObject(GetObstacle(
            this.objectPool.pop(),
            bounds[0] + Math.random() * (bounds[1] - 60),
            "boost",
            0
          ), 'backObjects');
        }

        else {
          this.addRoadObject(GetObstacle(
            this.objectPool.pop(),
            bounds[0] + Math.random() * (bounds[1] - 60),
            "oil",
            0
          ), 'backObjects');
        }

        this.curSpawnExtra = 5 + Math.random() * 10
      }
      else {
        this.trySpawnCar();
        this.curSpawnExtra -= 1;
      }

    }
  },

  getAllAvailables: function () {
    var aux = [];
    var tmp = this.roadStatus;

    while(tmp) {
      if (tmp.reachable()) aux.push(tmp);

      tmp = tmp.rightNode;
    }

    return aux;
  },

  trySpawnCar: function () {
    var availables = this.getAllAvailables();
    var selected;

    // Random a spawn position
    if (availables.length >= 2)
      selected = availables[Math.floor(Math.random() * availables.length)];

    else if (availables.length == 1) {
      selected = availables[0];

      // No available space to spawn
      if (selected.size() < player.width * 2) return;
    }

    else
      return console.log('SOMETHING IS WRONG');

    var x = selected.randomPartition(60, availables.length > 2);

    var car = GetObstacle(this.objectPool.pop(), x, "car");

    selected.split(x, x + car.width, car);
    this.addRoadObject(car);
  },

  addRoadObject: function(obj, where) {
    where = where || 'objects';
    this[where][obj.id] = obj;
  },

  recycleObject: function(obj) {
    this.objectPool.push(obj);
    this.removeRoadObject(obj);
  },

  removeRoadObject: function (obj) {
    delete this.objects[obj.id];
    delete this.backObjects[obj.id];
  },
}

function newRoadArea(start, finish, car) {
  var obj = _.clone(RoadArea);
  obj.init(start, finish, car);
  return obj;
}

var otherSide = {
  left: 'right',
  right: 'left',
}

// kind = 0: free, 1: cant reach, 2: occupied, 
var RoadArea = {
  init: function (start, finish, car) {
    this.leftPos = start;
    this.rightPos = finish;
    this.leftBorder = this.rightBorder = car;
    if (this.car) this.kind = 2;
    else this.kind = 0;

    this.leftNode = null;
    this.rightNode = null;
  },

  reset: function (start, finish, kind, leftCar, rightCar) {
    this.leftPos = start;
    this.rightPos = finish;
    this.leftBorder = leftCar;
    this.rightBorder = rightCar;
    this.kind = kind;
  },

  split: function (nstart, nfinish, car) {
    // Split current node
    var newRight = newRoadArea(nfinish, this.rightPos);
    var newLeft = newRoadArea(this.leftPos, nstart);
    this.reset(nstart, nfinish, 2, car, car);

    // relink linked list
    newRight.leftNode = this;
    newRight.rightNode = this.rightNode;
    newLeft.leftNode = this.leftNode;
    newLeft.rightNode = this;

    if (this.rightNode) this.rightNode.leftNode = newRight;
    if (this.leftNode) this.leftNode.rightNode = newLeft;

    this.rightNode = newRight;
    this.leftNode = newLeft;

    // make sure roadStatus always have the first node of the list
    var bounds = road.getFirstBounds();
    if (newLeft.leftPos == bounds[0])
      roadManager.roadStatus = newLeft;
  },

  join: function (side) {
    var node = this[side+'Node'];
    if (!node) return;
    
    // merge nodes
    this[side+'Pos'] = node[side+'Pos'];
    this[side+'Border'] = node[side+'Border'];
    this[side+'Node'] = node[side+'Node'];

    // make next node be consistent
    if (node[side+'Node'])
      node[side+'Node'][otherSide[side]+'Node'] = this;

    // make sure roadStatus always have the first node of the list
    var bounds = road.getFirstBounds();
    if (this.leftPos == bounds[0])
      roadManager.roadStatus = this;
  },

  update: function () {
    if (this.kind != 0) {
      this.kind = Math.min(this.checkSide('left'), this.checkSide('right'));

      this.updateSide('left');
      this.updateSide('right');
    }

    // update the whole list
    if (this.rightNode) this.rightNode.update();
  },

  checkSide: function (side) {
    var node = this[side+'Node'];
    if (!node) return 2;


    if (this.kind == 0 && this.reachable() && node.kind <= 1)
      return 0;

    var newKind = this.checkFree(this[side+'Border']);

    if (newKind == 0) {
      if (node.reachable()) return 0;
      return 1;
    }

    return newKind;
  },

  updateSide: function (side, kind) {
    var node = this[side+'Node'];
    if (!node) return 2;

    // Join into free zone
    if (this.kind == 0 && node.kind <= 1)
      this.join(side);

    // Join into unreachable zone
    else if (node && this.kind == 1 && node.absorbable())
      this.join(side);
  },

  absorbable: function () {
    return (this.kind == 0 && this.size() <= player.width) || this.kind == -1;
  },

  reachable: function() {
    return this.kind == 0 && this.size() > player.width;
  },

  size: function () {
    return this.rightPos - this.leftPos;
  },

  randomPartition: function (width, safe) {
    var x = this.leftPos + Math.random() * (this.rightPos - this.leftPos - width);

    if (safe) return x;

    // player fits in a splited zone
    if (x - this.leftPos > player.width || this.rightPos - x - width > player.width)
      return x;

    // return a pos we are sure we will generate a safe zone
    return this.leftPos;
  },

  checkFree: function(car) {
    if (!car) return 0;

    // if player can get behind this car
    if (car.y - player.height - (player.velY * (player.width / player.velX)) > 0)
      return 0;

    // if car already crossed the spawn area)
    else if (car.y > 0)
      return 1;

    return 2;
  },

  toString: function() {
    return '[' + this.leftPos + ',' + this.rightPos + ',' + this.kind + ']';
  },

  print: function () {
    console.log('[', this.leftPos, ',', this.rightPos, ',', this.kind, ']');

    // update the whole list
    if (this.rightNode) this.rightNode.print();
  },
}