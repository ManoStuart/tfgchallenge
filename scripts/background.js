var asphalt = {
  sprite: {draw: _.noop},
  line: {draw: _.noop},
  rightSide: {draw: _.noop},
  leftSide: {draw: _.noop},

  x: 0,
  y: 0,
  width: 60,
  height: 100,

  init: function (x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.sprite = spritesManager.newSprite("asphalt", width, height);
    this.line = spritesManager.newSprite("lines", width, height/2);
    this.rightSide = spritesManager.newSprite("beach", width/2, height);
    this.leftSide = spritesManager.newSprite("grass", width/2, height);
  },

  draw: function(context) {
    this.sprite.fullDraw(context, this.x, this.y);
    this.line.fullDraw(context, this.x, this.y);
    this.line.fullDraw(context, this.x, this.y + this.height/2);
    this.rightSide.fullDraw(context, this.x + this.width, this.y);
    this.leftSide.fullDraw(context, 0, this.y);
  },

  update: function(deltaTime) {
    this.y += player.velY * deltaTime;

    if (this.y > Game.canvas.height)
      road.nextFirst = this;
  },
};

function newAsphalt () {
  return _.clone(asphalt);
}

var road = {
  nextFirst: null,
  firstAsphalt: null,
  asphalts: [],

  width: 400,
  height: 200,

  init: function (canvas) {

    this.width = canvas.width / 2;
    this.height = this.width / 2;

    var n = canvas.height / this.height + 2
    for (var i = 0; i < n; i++) {
      this.asphalts[i] = newAsphalt();
      this.asphalts[i].init(
        this.width / 2,
        (-2 * this.height) + (i * this.height),
        this.width,
        this.height
      );
    }

    this.firstAsphalt = this.asphalts[0];
  },

  draw: function (context) {
    _.invoke(this.asphalts, 'draw', context);
  },

  update: function (deltaTime) {
    _.invoke(this.asphalts, 'update', deltaTime);

    if (this.nextFirst) {
      this.nextFirst.y = this.firstAsphalt.y - this.nextFirst.height;
      this.firstAsphalt = this.nextFirst;
      this.nextFirst = null;
    }
  },

  getFirstBounds: function () {
    return [this.firstAsphalt.x, this.firstAsphalt.width];
  },

  clampToBounds: function (x, y, width) {
    for (var i in this.asphalts) {
      var aux = this.asphalts[i];
      if (aux.y < y && aux.y + aux.height > y)
        return x.clamp(aux.x, aux.x + aux.width - width);
    }
  }
}