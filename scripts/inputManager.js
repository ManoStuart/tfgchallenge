var inputState = {
  steerLeft: false,
  steerRight: false
}

function inputInit (canvas) {
  $(document).keydown(function(e) {
    switch(e.which) {
      case 65: //a
      case 37: // left arrow
        inputState.steerLeft = true;
      break;

      case 68: //d
      case 39: // right arrow
        inputState.steerRight = true;
      break;

      case 27:
        inputState.pause = true;
      break;

      case 13:
        inputState.enter = true;
      break;

      default: return;
    }

    e.preventDefault();
  });

  $(document).keyup(function(e) {
    switch(e.which) {
      case 65: //a
      case 37: // left arrow
        inputState.steerLeft = false;
      break;

      case 68: //d
      case 39: // right arrow
        inputState.steerRight = false;
      break;

      case 27:
        inputState.pause = false;
      break;

      case 13:
        inputState.enter = false;
      break;

      default: return;
    }

    e.preventDefault();
  });

  canvas.addEventListener('mousedown', function() {
    var x = event.pageX - canvas.offsetLeft;

    if (x > canvas.width / 2)
      inputState.steerRight = true;
    else
      inputState.steerLeft = true;

  }, false);

  canvas.addEventListener('mouseup', function() {
    inputState.steerRight = false;
    inputState.steerLeft = false;
  }, false);
}