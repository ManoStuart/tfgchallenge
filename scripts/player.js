var player = {
  x: 0,
  y: 0,
  width: 60,
  height: 120,
  curAngle: 0,

  velY: 100,
  maxVelY: 600,
  acceleration: 400,

  velX: 300,

  childs: {},

  score: 0,

  init: function (canvas) {
    this.x = canvas.width / 2 - this.width / 2;
    this.y = canvas.height - this.height - 50;

    this.sprite = spritesManager.newAnimatedSprite("white_car", this.width, this.height, true, [0,1]);

    this.engineLoop = this.engineLoop || Sound('engine_loop.wav', true);
    this.crashSound = this.crashSound || Sound('crash.wav', false);
    this.engineLoop.play();

    _.extend(this, defaultState);
    this.initState();
  },

  reset: function (canvas) {
    this.x = canvas.width / 2 - this.width / 2;
    this.y = canvas.height - this.height - 50;

    _.extend(this, defaultState);
    this.initState();

    this.score = 0;
    this.curAngle = 0;
    this.maxVelY = 600;
    this.childs = {};

    this.engineLoop.play();
  },

  draw: function(context) {
    this.sprite.startDraw(context, this.x, this.y, this.curAngle);

    this.sprite.draw(context);
    _.invoke(this.childs, 'draw', context);

    this.sprite.endDraw(context);
  },

  removeChild: function(childId) {
    delete this.childs[childId];
  },

  addChild: function(child) {
    this.childs[child.id] = child;
  },

  checkCollisions: function () {
    var obj = _.find(roadManager.objects, this.checkCollision, this);
    obj = obj || _.find(roadManager.backObjects, this.checkCollision, this);

    if (obj) {
      if (obj.type == 'car')
        this.onCarColision(obj);
      else if (obj.type == 'oil')
        this.onOilColision(obj);
      else if (obj.type == 'boost')
        this.onBoostColision(obj);
      else if (obj.type == 'hole')
        this.onHoleColision(obj);
    }
  },

  checkCollision: function (obj) {
    return !obj.collided &&
           this.x < obj.x + obj.width &&
           this.x + this.width > obj.x &&
           this.y < obj.y + obj.height &&
           this.y + this.height > obj.y;
  },

  onBoostColision: function(boost) {
    boost.collided = true;
    this.changeState(boostState);
  },

  onOilColision: function(oil) {
    oil.collided = true;
    this.changeState(oilState);
  },

  onCarColision: function(car) {
    car.collided = true;

    this.changeState(deathState);
    _.extend(car, deathState);

    if (this.y > car.y) {
      this.sprite.play([8,9], true, 12);
      car.sprite.play([6,7], true, 12);
    }
    else {
      this.sprite.play([6,7], true, 12);
      car.sprite.play([8,9], true, 12);
    }

    car.initState(this.velY);

    roadManager.addRoadObject(
      newEffect('explosion', (this.x + car.x) / 2, (this.y + car.y) / 2, 64, 64, undefined, this.velY / 2, false)
    );

    this.engineLoop.pause();
    this.crashSound.play();
    Game.end();
  },

  changeState: function (state) {
    this.destroyState();
    _.extend(this, state);
    this.initState();
  },
};

var defaultState = {
  initState: function (velY) {
    this.curSprite = 0;
    this.sprite.play([0,1], true);
  },

  update: function(deltaTime) {
    this.sprite.update(deltaTime);
    _.invoke(this.childs, "update", deltaTime);
    this.score += 10 * deltaTime;

    this.velY += this.acceleration * deltaTime;
    this.velY = this.velY.clamp(0, this.maxVelY);

    this.maxVelY += 3 * deltaTime;

    // process input and change animation
    if (inputState.steerRight) {
      this.x += this.velX * deltaTime;
      if (this.curSprite != 1) {
        this.sprite.play([2,3], true);
        this.curSprite = 1;
      }
    }

    else if (inputState.steerLeft) {
      this.x -= this.velX * deltaTime;
      if (this.curSprite != 2) {
        this.sprite.play([4,5], true);
        this.curSprite = 2;
      }
    }

    else if (this.curSprite != 0) {
      this.curSprite = 0;
      this.sprite.play([0,1], true);
    }

    // Clamp x position
    this.x = road.clampToBounds(this.x, this.y, this.width);

    // colision detection
    this.checkCollisions();
  },

  destroyState: _.noop
}

var deathState = {
  curAngle: 0,
  angleVel: 0,

  initState: function(velY) {
    this.velY = velY || this.velY;
    this.angleVel = Math.random() * 150 - 75;
  },

  update: function(deltaTime) {
    this.sprite.update(deltaTime);
    _.invoke(this.childs, "update", deltaTime);

    this.velY *= 0.98;
    this.angleVel *= 0.98;

    this.curAngle += this.angleVel * deltaTime;
  },

  destroyState: _.noop
}

var oilState = {
  fixedSteer: 0,
  stateDuration: 0.8,

  initState: function(velY) {
    this.fixedSteer = Math.random() * 200 - 100;

    if (this.fixedSteer > 0)
      this.sprite.play([2,3], true);
    else
      this.sprite.play([4,5], true);

    this.driftSound = this.driftSound || Sound('drift.wav', false);
    this.driftSound.play();
  },

  update: function(deltaTime) {
    this.sprite.update(deltaTime);
    _.invoke(this.childs, "update", deltaTime);
    this.score += 10 * deltaTime;

    // fixed velY.
    // apply fixed steer direction
    this.x += this.fixedSteer * deltaTime;

    // Clamp x position
    this.x = road.clampToBounds(this.x, this.y, this.width);

    // colision detection
    this.checkCollisions();

    // Regain control
    this.stateDuration -= deltaTime;
    if (this.stateDuration <= 0) {
      this.changeState(defaultState);
    }
  },
}

var boostState = {
  bonusVelY: 200,
  stateDuration: 3,
  boostEffect: -1,

  initState: function(velY) {
    var child = newEffect('fire', -30, this.height / 2 - 10, 60, 60, this, 0, true);
    this.boostEffect = child.id;
    this.addChild(child);
    this.score += 150;

    this.nitroSound = this.nitroSound || Sound('nitro.wav', false);
    this.nitroSound.play();
  },

  update: function(deltaTime) {
    this.sprite.update(deltaTime);
    _.invoke(this.childs, "update", deltaTime);
    this.score += 10 * deltaTime;

    this.velY += this.acceleration * deltaTime;
    this.velY = this.velY.clamp(0, this.maxVelY + this.bonusVelY);

    this.maxVelY += 3 * deltaTime;

    // process input and change animation
    if (inputState.steerRight) {
      this.x += this.velX * deltaTime;
      if (this.curSprite != 1) {
        this.sprite.play([2,3], true);
        this.curSprite = 1;
      }
    }

    else if (inputState.steerLeft) {
      this.x -= this.velX * deltaTime;
      if (this.curSprite != 2) {
        this.sprite.play([4,5], true);
        this.curSprite = 2;
      }
    }

    else if (this.curSprite != 0) {
      this.curSprite = 0;
      this.sprite.play([0,1], true);
    }

    // Clamp x position
    this.x = road.clampToBounds(this.x, this.y, this.width);

    // colision detection
    this.checkCollisions();

    // Remove boost
    this.stateDuration -= deltaTime;
    if (this.stateDuration <= 0) {
      this.changeState(defaultState);
    }
  },

  destroyState: function () {
    this.removeChild(this.boostEffect);
  },
}