var obstacle = {
  sprite: {draw: $.noop},
  x: 0,
  y: 0,
  width: 60,
  height: 120,
  curAngle: 0,
  id: -1,

  velY: 0,
  collided: false,

  init: function (x, type, velY) {
    this.id = ++nextObjectId;
    this.type = type;

    this.x = x;
    this.y = -this.height;
    this.velY = (velY !== undefined) ? velY : Math.max(player.maxVelY * 0.7, 420);

    this.collided = false;
  },

  draw: function(context) {
    this.sprite.startDraw(context, this.x, this.y, this.curAngle);
    this.sprite.draw(context);
    this.sprite.endDraw(context);
  },

  update: function(deltaTime) {
    this.sprite.update(deltaTime);

    this.y += (player.velY - this.velY) * deltaTime;

    if (this.y > Game.canvas.height)
      roadManager.recycleObject(this);
  },
};

function initObstacles() {
  obstacleTypes = {
    car: {
      sprite: function () {this.sprite = spritesManager.newAnimatedSprite("yellow_car", 60, 120, true, [0,1])},
      width: 60,
      height: 120,
    },

    oil: {
      sprite: function () {this.sprite = spritesManager.newAnimatedSprite("oil", 60, 33)},
      width: 60,
      height: 33,
    },

    boost: {
      sprite: function () {this.sprite = spritesManager.newAnimatedSprite("boost", 60, 156)},
      width: 60,
      height: 156,
    },

    hole: {
      sprite: function () {this.sprite = spritesManager.newAnimatedSprite("oil", 60, 33)},
      width: 60,
      height: 33,
    }
  }
}

function newObstacle() {
  return _.clone(obstacle);
}

function GetObstacle(obj, x, type, velY) {
  obj = obj || newObstacle();

  _.extend(obj, obstacleTypes[type]);
  obj.sprite();

  obj.init(x, type, velY);

  return obj;
}