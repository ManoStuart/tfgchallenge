var label = {
  init: function (x, y, width, height, text, textColor, background, font) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.text = text;
    this.textColor = textColor || '#000';
    this.background = (background === undefined) ? '#fff' : background;
    this.font = font;
  },

  draw: function(context) {
    context.save();

    if (this.background) {
      if (this.width == Game.canvas.width)
        context.globalAlpha = 0.5;

      context.fillStyle = this.background;
      context.fillRect(this.x, this.y, this.width, this.height);
    }

    if (this.text) {
      context.font = this.font;
      context.fillStyle = this.textColor;
      context.fillText(this.text, this.x + 10, this.y + this.height / 2);
    }

    context.restore();
  },

  setText: function (text) {
    this.text = text;
  },
}

function newLabel (x, y, width, height, text, textColor, background, font) {
  var obj = _.clone(label);
  obj.init(x, y, width, height, text, textColor, background, font);
  return obj;
}

var hud = {
  labels: [],

  init: function (canvas) {
    this.labels = [newLabel(20, 0, 200, 50, 'score: 0', '#fff', '#000', "bold 30px Verdana")];
  },

  draw: function(context) {
    _.invoke(this.labels, 'draw', context);
  },

  update: function(deltaTime) {
    if (this.labels.length > 1) {
      if (inputState.enter)
        Game.restart();
    }
    else
      this.labels[0].setText('score: ' + Math.floor(player.score));
  },

  reset: function () {
    this.labels = [newLabel(20, 0, 200, 50, 'score: 0', '#fff', '#000', "30px Verdana")];
  },

  newHighscore: function () {
    this.labels = [
      newLabel(Game.canvas.width / 2 - 300, Game.canvas.height / 2 + 50, 600, 200, 'score: ' + Math.floor(player.score), '#000', null, "bold 55px Verdana"),
      newLabel(Game.canvas.width / 2 - 300, Game.canvas.height / 2 - 200, 600, 200, 'NEW HIGHSCORE!!', '#fff', '#f00', "bold 60px Verdana"),
      newLabel(0, 0, Game.canvas.width, Game.canvas.height, null, null, '#000')];
  },

  endGame: function () {
    this.labels = [
      newLabel(Game.canvas.width / 2 - 300, Game.canvas.height / 2 + 50, 600, 200, 'score: ' + Math.floor(player.score), '#000', null, "bold 55px Verdana"),
      newLabel(Game.canvas.width / 2 - 300, Game.canvas.height / 2 - 200, 600, 200, 'highscore: ' + Math.floor(Game.highscore), '#000', '#fff', "bold 40px Verdana"),
      newLabel(0, 0, Game.canvas.width, Game.canvas.height, null, null, '#000')];
  }
};