var Game = {};

Game.fps = 30;
Game.highscore = 0;

paused = false;
justPaused = false;

Game.initialize = function() {
  this.canvas = document.getElementById("canvas");
  this.context = this.canvas.getContext("2d");

  inputInit(this.canvas);

  spritesManager.preloadImage("white_car", 200, 494.5, true, 15);
  spritesManager.preloadImage("yellow_car", 200, 494.5, true, 15);
  spritesManager.preloadImage("lines", 1024, 256, false, 0);
  spritesManager.preloadImage("beach", 256, 256, false, 0);
  spritesManager.preloadImage("grass", 256, 256, false, 0);
  spritesManager.preloadImage("explosion", 96, 96, false, 20);
  spritesManager.preloadImage("oil", 134, 73, false, 0);
  spritesManager.preloadImage("boost", 60, 156, true, 6);
  spritesManager.preloadImage("fire", 96, 192, true, 20);
  spritesManager.preloadImage("asphalt", 2048, 1024, false, 0);

  road.init(this.canvas);
  player.init(this.canvas);
  roadManager.init(this.canvas, 8);
  hud.init(this.canvas);

  this.music = this.music || Sound('music.ogg', true);
  this.music.play();
};


Game.draw = function() {
  if (paused) return;

  this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

  road.draw(this.context);
  roadManager.draw(this.context);
  player.draw(this.context);
  hud.draw(this.context);
};


Game.update = function(deltaTime) {
  if (inputState.pause && !justPaused) {
    paused = !paused;
    justPaused = true;

    if (paused) {
      this.music.pause();
      player.engineLoop.pause();
    }
    else  {
      this.music.play();
      player.engineLoop.play();
    }
  }
  if (!inputState.pause)
    justPaused = false;

  if (paused) return;

  road.update(deltaTime);
  player.update(deltaTime);
  roadManager.update(deltaTime);
  hud.update(deltaTime);
};

Game.end = function () {
  roadManager.active = false;

  if (player.score > Game.highscore) {
    Game.highscore = player.score;
    hud.newHighscore();
  }
  else {
    hud.endGame();
  }
}

Game.restart = function () {
  player.reset(this.canvas);
  roadManager.reset();
  hud.reset();
}