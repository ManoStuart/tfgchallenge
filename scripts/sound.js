function Sound (name, loop) {
  var sound = $('<audio />').get(0);
  sound.src = "sounds/" + name;
  sound.loop = loop;

  return sound;
}