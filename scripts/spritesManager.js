var images = {};

function newSpriteSheet(image, sWidth, sHeight, loop, fps, numFrames) {
  return {
    image: image,
    sWidth: sWidth || image.width,
    sHeight: sHeight || image.height,
    numColums: image.width / sWidth,
    numFrames: numFrames || (image.width / sWidth) * (image.height / sHeight),
    loop: loop || false,
    fps: fps || 20,
  }
}

var animatedSprite = {
  init: function (sheet, dWidth, dHeight, loop, pattern, fps) {
    this.pattern = pattern || null;
    this.curFrame = (pattern) ? pattern[0] : 0;
    this.curPattern = 0;
    this.active = true;
    this.cd = 0;
    this.loop = loop;
    this.dWidth = dWidth;
    this.dHeight = dHeight;

    this.sheet = sheet;
    if (!sheet) {
      this.active = false;
      return;
    }

    this.fps = fps || this.sheet.fps;
    this.loop = loop || this.sheet.loop;
  },

  play: function (pattern, loop, fps, start) {
    this.pattern = pattern;
    this.curPattern = start || 0;
    this.curFrame = pattern[this.curPattern];
    this.cd = 0;
    this.loop = loop || this.loop;
    this.fps = fps || this.fps;
  },

  setImage: function (sheet) {
    this.sheet = sheet;
    _.defaults(this, _.pick(this.sheet, 'fps', 'loop', 'sWidth', 'sHeight'));
    this.active = true;
  },

  startDraw: function(context, x, y, angle) {
    if (!this.sheet) return;
    context.save();

    this.sx = (this.curFrame % this.sheet.numColums) * this.sheet.sWidth;
    this.sy = Math.floor(this.curFrame / this.sheet.numColums) * this.sheet.sHeight;

    context.translate(x + this.dWidth / 2, y + this.dHeight / 2);
    context.rotate((angle || 0) * Math.PI / 180);
  },

  draw: function(context) {
    if (!this.sheet) return;

    context.drawImage(
      this.sheet.image,
      this.sx,
      this.sy,
      this.sheet.sWidth,
      this.sheet.sHeight,
      -this.dWidth / 2,
      -this.dHeight / 2,
      this.dWidth,
      this.dHeight
    );
  },

  endDraw: function(context) {
    context.restore();
  },

  fullDraw: function(context, x, y, angle) {
    this.startDraw(context, x, y, angle);
    this.draw(context);
    this.endDraw(context);
  },

  update: function(deltaTime) {
    if (!this.active || !this.fps)
      return;

    this.cd += deltaTime;
    if (this.cd > 1 / this.fps) {
      this.nextFrame();

      this.cd -= 1 / this.fps;
    }
  },

  nextFrame: function () {
    if (this.pattern) {
      if (!this.loop && this.curPattern == this.pattern.length - 1) {
        this.active = false;
        if (this.notify) this.notify();
      }

      this.curPattern = (this.curPattern + 1) % this.pattern.length;
      this.curFrame = this.pattern[this.curPattern];
    }
    else {
      if (!this.loop && this.curFrame == this.sheet.numFrames - 1) {
        this.active = false;
        if (this.notify) this.notify();
      }

      this.curFrame = (this.curFrame + 1) % this.sheet.numFrames;
    }
  },

  setFrame: function (frame) {
    this.curFrame = frame % this.sheet.numFrames;
  },

  registerNotify: function (notify) {
    this.notify = notify;
  }
}

var spritesManager = {
  images: {},
  onHold: {},

  preloadImage: function (name, sWidth, sHeight, loop, fps, numFrames) {
    var img = new Image();
    
    var self = this;

    img.onload = function() {
      spritesManager.images[name] = newSpriteSheet(this, sWidth, sHeight, loop, fps, numFrames);

      _.invoke(self.onHold[name], "setImage", spritesManager.images[name]);
    };
    
    img.src = "images/" + name + ".png";
  },

  newSprite: function(imgName, dWidth, dHeight) {
    return this.newAnimatedSprite(imgName, dWidth, dHeight, false, undefined, 0);
  },

  newAnimatedSprite: function (imgName, dWidth, dHeight, loop, pattern, fps) {
    var image = this.images[imgName];

    var obj = _.clone(animatedSprite);

    obj.init(image, dWidth, dHeight, loop, pattern, fps);

    if (!image) {
      if (!this.onHold[imgName]) this.onHold[imgName] = [];
      this.onHold[imgName].push(obj);
    }

    return obj;
  }
}